﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	//Fahrkartenautomat durchlauf
        //==========================================
    	do {    		
    	
       Scanner tastatur = new Scanner(System.in);
      
       double eingezahlterGesamtbetrag; //checkt immer wieder welcher betrag eingezahlt wurde und führt entsprechende operationen durch bzw zieht dies vom noch zu zahlenden betrag ab und addiert die eingeworfene münzen drauf
       //int - ganze zahlen zum darstellen von klaren anzahlen, z.b. biologisch vorgeben - ein mensch besteht aus nur einem teil
       //double - gleitkommazahlen zum darstellen von teilen von einheiten und zerlegen in kleinere datenzeichen          
                  
       //Fahrkarten auswählen und bestellen
       //==========================================
      double zuZahlenderBetrag = fahrkartenBestellungErfassen(tastatur);
      
       // Geldeinwurf      
       // -----------
      eingezahlterGesamtbetrag = fahrkartenBezahlen(tastatur, zuZahlenderBetrag);         
       
       // Fahrscheinausgabe
       // ----------------- 
      fahrkartenAusgabe();
       
       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
      geldRückgabe (eingezahlterGesamtbetrag, zuZahlenderBetrag);    		  
       
       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.\n\n");
    	}while(true);
       
    }
    
    private static double fahrkartenBestellungErfassen(Scanner bestellung) {       
        
        double fahrkartenauswahl;
        double fahrkarte = 0;
        
        System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus: \n");
        System.out.println("Einzelfahrschein Regeltarif AB [2,90 €]	(1)");
        System.out.println("Tageskarte Regeltarif AB [8,60 €] (2)");
        System.out.println("Kleingruppen-Tageskarte Regeltarif AB [23,50 €] (3) \n");
        fahrkartenauswahl = bestellung.nextDouble();
        
        while (fahrkartenauswahl < 1 || fahrkartenauswahl > 3) {
        	System.out.println("Bitte geben Sie eine Auswahl von 1, 2 oder 3 an.");
        	 fahrkartenauswahl = bestellung.nextDouble();
        }
        if (fahrkartenauswahl == 1) {
        	fahrkarte = 2.90;
        	
        	}
        if (fahrkartenauswahl == 2) {
        	fahrkarte = 8.60;
        	
        	}
        if (fahrkartenauswahl == 3) {
        	fahrkarte = 23.50;	
			}
        
        System.out.print("Gewünschte Anzahl der Tickets: ");
	    int anzahltickets = bestellung.nextInt();

        while (anzahltickets < 1 || anzahltickets > 10) {
        	System.out.println("Ungültige Eingabe! Bitte treffen Sie eine Auswahl zwischen 1 und 10 an.");
        	anzahltickets = bestellung.nextInt();
        }
	    
	    double gesamtPreis = (double)anzahltickets * fahrkarte; //hier werden die eingabe der ticketanzahl und der zu zahlende betrag multipliziert
	    //das double davor bestimmt die variable anzahltickets in dieser rechnung als double - auch wenn es nicht nötig wäre da durch die andere variable double genommen worden wäre
	    //es wird automatische die kleinste einheit verwendet, dennoch stellt man auf diese weise sicher, dass es nicht zu unvorhergesehenden ereignissen kommt
	    System.out.printf("Zu zahlender Betrag (EURO): %.2f %n", gesamtPreis);	
		return gesamtPreis;
        
        }
        
    
    static double fahrkartenBezahlen(Scanner geldeinwurf, double zuZahlen) {
		
		double eingezahlt = 0.0;
		double eingeworfeneMünze;
		while(eingezahlt < zuZahlen)	
		{
	      //   System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag) + " €");
	    	   System.out.printf("Noch zu zahlen: %.2f €  %n", (zuZahlen - eingezahlt)); //formatierung auf 2 kommastellen
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	   eingeworfeneMünze = geldeinwurf.nextDouble();
	           eingezahlt += eingeworfeneMünze;
		}
	           
		return eingezahlt;
	}
    
    
    static void fahrkartenAusgabe() {
		
		System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");		
		
	}
    
	
    static double geldRückgabe(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		
	double rückgabebetrag; //wenn der gesamtbetrag>zuzahlender ist dann wird dieser wert berechnet und ausgegeben
		 
		rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       if(rückgabebetrag > 0.0)
	       {
	    	//   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
	    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro " , rückgabebetrag);
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 1.995) // 2 EURO-Münzen
	           {
	        	  System.out.println("2,00 €");
		          rückgabebetrag -= 2.0;
	           }
	           while(rückgabebetrag >= 0.995) // 1 EURO-Münzen
	           {
	        	  System.out.println("1,00 €");
		          rückgabebetrag -= 1.0;
	           }
	           while(rückgabebetrag >= 0.495) // 50 CENT-Münzen
	           {
	        	  System.out.println("0,50 €");
		          rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag >= 0.195) // 20 CENT-Münzen
	           {
	        	  System.out.println("0,20 €");
	 	          rückgabebetrag -= 0.2;
	           }
	           while(rückgabebetrag >= 0.095) // 10 CENT-Münzen
	           {
	        	  System.out.println("0,10 €");
		          rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.004)// 5 CENT-Münzen
	           {
	        	  System.out.println("0,05 €");
	 	          rückgabebetrag -= 0.05;
	           }
	       }

		return rückgabebetrag;
	}
}