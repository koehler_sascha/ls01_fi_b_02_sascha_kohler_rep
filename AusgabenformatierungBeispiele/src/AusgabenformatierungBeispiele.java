
public class AusgabenformatierungBeispiele {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	//	System.out.println("hello");
	//	System.out.print("du \"alex\"");
		
	//String
	/*	System.out.printf("|%s|%n", "123456789");
		System.out.printf("|%20s|\n", "123456789");
		System.out.printf("|%-20s|\n", "123456789");
		System.out.printf("|%-20.3s|\n", "123456789");
	*/	
		
	//Ganze Zahl
		
	/*	System.out.printf("|%d|%n",123456789);
		System.out.printf("|%20d|%n",123456789);
		System.out.printf("|%-20d|%n",123456789);
		System.out.printf("|%-20d|%n",123456789);
		System.out.printf("|%+-20d|%n",123456789);
		System.out.printf("|%+-20d|%n",-123456789);
		System.out.printf("|%+020d|%n",123456789);
	*/
		
	//Flie�kommazahlen
		
		System.out.printf("|%f|%n",123456.789);
		System.out.printf("|%.2f|%n",123456.789);
		System.out.printf("|%20.2f|%n",123456.789);
		System.out.printf("|%-20.2f|%n",123456.789);
		System.out.printf("|%020.2f|%n",123456.789);
		System.out.printf("|%+020.2f|%n",-123456.789);
		System.out.printf("|%+020.2f|%n",123456.789);
	}

}
