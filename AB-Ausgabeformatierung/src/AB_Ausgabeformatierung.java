
public class AB_Ausgabeformatierung {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	//Aufgabe 1
	/*	System.out.printf("%12s%n", "**     ");
		System.out.printf("%-6s", "*");
		System.out.printf("%6s%n", "*");
		System.out.printf("%-6s", "*");
		System.out.printf("%6s%n", "*");
		System.out.printf("%12s%n", "**     ");
	*/
		
	//Aufgabe 2
		
		System.out.printf("%-5s", "!0");
		System.out.printf("%s", "=");
		System.out.printf("%-19s", "");
		System.out.printf("%s", "=");
		System.out.printf("%4s%n", "1");
		
		System.out.printf("%-5s", "!1");
		System.out.printf("%s", "=");
		System.out.printf("%-19s", " 1");
		System.out.printf("%s", "=");
		System.out.printf("%4s%n", "1");
		
		System.out.printf("%-5s", "!2");
		System.out.printf("%s", "=");
		System.out.printf("%-19s", " 1 * 2");
		System.out.printf("%s", "=");
		System.out.printf("%4s%n", "2");
		
		System.out.printf("%-5s", "!3");
		System.out.printf("%s", "=");
		System.out.printf("%-19s", " 1 * 2 * 3");
		System.out.printf("%s", "=");
		System.out.printf("%4s%n", "6");
		
		System.out.printf("%-5s", "!4");
		System.out.printf("%s", "=");
		System.out.printf("%-19s", " 1 * 2 * 3 * 4");
		System.out.printf("%s", "=");
		System.out.printf("%4s%n", "24");
		
		System.out.printf("%-5s", "!5");
		System.out.printf("%s", "=");
		System.out.printf("%-19s", " 1 * 2 * 3 * 4 * 5");
		System.out.printf("%s", "=");
		System.out.printf("%4s%n", "120");
		
	}

}
